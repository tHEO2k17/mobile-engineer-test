package com.quandoo.androidtask.data.model.response

data class RestaurantData(
    val tables: List<Table>,
    val customers: List<Customer>,
    val reservations: List<Reservation>
)
