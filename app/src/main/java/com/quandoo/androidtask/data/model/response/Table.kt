package com.quandoo.androidtask.data.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Table(
    @SerializedName("shape") var shape: String = "",
    @SerializedName("id") var id: Long = 0,
    var reservedBy: String? = null
): Serializable