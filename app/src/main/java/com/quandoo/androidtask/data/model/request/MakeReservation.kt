package com.quandoo.androidtask.data.model.request

import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.data.model.response.Table

data class MakeReservation(
    val table: Table,
    val customer: Customer,
)
