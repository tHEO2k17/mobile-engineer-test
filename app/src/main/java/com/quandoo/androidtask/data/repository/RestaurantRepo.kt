package com.quandoo.androidtask.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.quandoo.androidtask.core.utils.Constants
import com.quandoo.androidtask.core.utils.FileStorageUtil
import com.quandoo.androidtask.data.datasource.remote.RestaurantService
import com.quandoo.androidtask.data.model.request.MakeReservation
import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.data.model.response.Reservation
import com.quandoo.androidtask.data.model.response.RestaurantData
import com.quandoo.androidtask.data.model.response.Table
import com.quandoo.androidtask.domain.repository.IRestaurantRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RestaurantRepo
@Inject constructor(
    private val restaurantService: RestaurantService,
    private val fileStorageUtil: FileStorageUtil
) : IRestaurantRepo {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun fetchTables(): MutableList<Table> {
        return try {

            val tables = mutableListOf<Table>()
            val storedTables = fileStorageUtil
                .readSerializable<ArrayList<Table>>(Constants.TABLES_FILE_NAME)

            if (storedTables != null) tables.addAll(storedTables)
            else {
                compositeDisposable.add(restaurantService
                    .getTables()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { tablesData ->
                        if (tablesData != null) {
                            fileStorageUtil.saveSerializable(
                                tablesData as ArrayList<Table>,
                                Constants.TABLES_FILE_NAME
                            )
                            tables.addAll(tablesData)
                        }
                    })
            }

            tables
        } catch (e: Exception) {
            print(e.localizedMessage)
            mutableListOf()
        }
    }

    override fun fetchCustomers(): MutableList<Customer> {
        return try {

            val customers = mutableListOf<Customer>()
            val storedCustomers = fileStorageUtil
                .readSerializable<ArrayList<Customer>>(Constants.CUSTOMERS_FILE_NAME)

            if (storedCustomers != null) customers.addAll(storedCustomers)
            else {
                compositeDisposable.add(restaurantService.getCustomers()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { customersData ->
                        if (customersData != null) {
                            fileStorageUtil.saveSerializable(
                                customersData as ArrayList<Customer>,
                                Constants.CUSTOMERS_FILE_NAME
                            )
                            customers.addAll(customersData)
                        }
                    })
            }

            customers
        } catch (e: Exception) {
            print(e.localizedMessage)
            mutableListOf()
        }
    }

    override fun fetchReservations(): MutableLiveData<MutableList<Reservation>> {
        return try {

            val reservations = MutableLiveData<MutableList<Reservation>>()
            val storedReservations = fileStorageUtil
                .readSerializable<ArrayList<Reservation>>(Constants.RESERVATIONS_FILE_NAME)

            if (storedReservations != null) reservations.value = storedReservations
            else {
                compositeDisposable.add(restaurantService.getReservations()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { reservationsData ->
                        if (reservationsData != null) {
                            fileStorageUtil.saveSerializable(
                                reservationsData as ArrayList<Reservation>,
                                Constants.RESERVATIONS_FILE_NAME
                            )
                            reservations.value = reservationsData
                        }
                    })
            }

            reservations
        } catch (e: Exception) {
            print(e.localizedMessage)
            MutableLiveData<MutableList<Reservation>>()
        }
    }

    override fun fetchAllRestaurantData(): RestaurantData {
        return try {
            var allRestaurantData = RestaurantData(listOf(), listOf(), listOf())

            Observable.zip(
                restaurantService.getTables(),
                restaurantService.getCustomers(),
                restaurantService.getReservations()
            ) { tables, customers, reservations ->
                RestaurantData(tables, customers, reservations)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> allRestaurantData = result },
                    { error -> Log.e("REST REPO", "error->$error") }
                )

            allRestaurantData
        } catch (e: Exception) {
            print(e.localizedMessage)
            RestaurantData(listOf(), listOf(), listOf())
        }
    }

    override fun makeReservation(makeReservationDto: MakeReservation) {
        try {
            val tables = fileStorageUtil
                .readSerializable<ArrayList<Table>>(Constants.TABLES_FILE_NAME)
            val reservations = fileStorageUtil
                .readSerializable<ArrayList<Reservation>>(Constants.RESERVATIONS_FILE_NAME)
                ?: arrayListOf()

            if (tables != null) {
                tables.find { table -> table.id == makeReservationDto.table.id }
                    ?.let { table ->
                        reservations.add(
                            Reservation(
                                makeReservationDto.customer.id,
                                table.id,
                                makeReservationDto.customer.id + table.id
                            )
                        )
                        table.reservedBy =
                            "${makeReservationDto.customer.firstName} ${makeReservationDto.customer.lastName}"
                    }

                Log.d("REPO", "Updated tables: $tables")
                fileStorageUtil.saveSerializable(tables, Constants.TABLES_FILE_NAME)
                fileStorageUtil.saveSerializable(reservations, Constants.RESERVATIONS_FILE_NAME)
            }
        } catch (e: Exception) {
            Log.e("REPO", e.localizedMessage!!)
        }

    }

    override fun cancelReservation(reservedTable: Table) {
        try {
            val tables = fileStorageUtil
                .readSerializable<ArrayList<Table>>(Constants.TABLES_FILE_NAME)

            if (tables != null) {
                tables.find { table -> table.id == reservedTable.id }
                    ?.let { table -> table.reservedBy = null }

                fileStorageUtil.saveSerializable(tables, Constants.TABLES_FILE_NAME)
            }
        } catch (e: Exception) {
            Log.e("REPO", e.localizedMessage!!)
        }

    }
}