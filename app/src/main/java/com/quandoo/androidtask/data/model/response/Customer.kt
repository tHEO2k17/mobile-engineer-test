package com.quandoo.androidtask.data.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Customer(
    @SerializedName("first_name")
    var firstName: String? = null,
    @SerializedName("last_name")
    var lastName: String? = null,
    @SerializedName("image_url")
    var imageUrl: String? = null,
    @SerializedName("id")
    var id: Long = 0
): Serializable