package com.quandoo.androidtask.data.datasource.remote

import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.data.model.response.Reservation
import com.quandoo.androidtask.data.model.response.Table
import io.reactivex.Observable
import retrofit2.http.GET

interface RestaurantService {
    @GET("/quandoo-assessment/customers.json")
    fun getCustomers(): Observable<MutableList<Customer>>

    @GET("/quandoo-assessment/reservations.json")
    fun getReservations(): Observable<MutableList<Reservation>>

    @GET("/quandoo-assessment/tables.json")
    fun getTables(): Observable<MutableList<Table>>
}