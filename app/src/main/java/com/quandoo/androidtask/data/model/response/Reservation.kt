package com.quandoo.androidtask.data.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Reservation(
    @SerializedName("user_id") var userId: Long,
    @SerializedName("table_id") var tableId: Long,
    @SerializedName("id") var id: Long
): Serializable