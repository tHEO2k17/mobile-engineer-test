package com.quandoo.androidtask.core.utils

import android.util.Log

interface Logger {
    fun log(msg: String?) {
        Log.d(LOG_TAG, msg!!)
    }

    fun error(msg: String?) {
        Log.e(LOG_TAG, msg!!)
    }

    companion object {
        const val LOG_TAG = "TestTask"
    }
}