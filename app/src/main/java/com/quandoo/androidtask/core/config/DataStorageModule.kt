package com.quandoo.androidtask.core.config

import android.content.Context
import com.quandoo.androidtask.core.utils.FileStorageUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataStorageModule {

    @Singleton
    @Provides
    fun getFileStorage(@ApplicationContext context: Context): FileStorageUtil {
        return FileStorageUtil(context)
    }
}