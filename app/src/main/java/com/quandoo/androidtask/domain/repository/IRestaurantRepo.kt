package com.quandoo.androidtask.domain.repository

import androidx.lifecycle.MutableLiveData
import com.quandoo.androidtask.data.model.request.MakeReservation
import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.data.model.response.Reservation
import com.quandoo.androidtask.data.model.response.RestaurantData
import com.quandoo.androidtask.data.model.response.Table

interface IRestaurantRepo {
    fun fetchTables(): MutableList<Table>
    fun fetchCustomers(): MutableList<Customer>
    fun fetchReservations(): MutableLiveData<MutableList<Reservation>>
    fun fetchAllRestaurantData(): RestaurantData?
    fun makeReservation(makeReservationDto: MakeReservation)
    fun cancelReservation(reservedTable: Table)
}