package com.quandoo.androidtask.domain.interceptors

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptor(context: Context) : Interceptor {

    private val appContext = context.applicationContext

    override fun intercept(chain: Chain): Response {
        if (!isConnected()) throw IOException()

        return chain.proceed(chain.request())
    }

    private fun isConnected(): Boolean {
        try {
            val connectivityManager =
                appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                    ?.let {
                        it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                                it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                                it.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) ||
                                it.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ||
                                it.hasTransport(NetworkCapabilities.TRANSPORT_VPN)
                    } ?: false
            } else {
                @Suppress("DEPRECATION")
                return connectivityManager.activeNetworkInfo?.isConnectedOrConnecting == true
            }
        } catch (e: Exception) {
            return false
        }
    }

}