package com.quandoo.androidtask.presentation.views.tables

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.quandoo.androidtask.R
import com.quandoo.androidtask.core.utils.Logger
import com.quandoo.androidtask.data.model.response.Table
import com.quandoo.androidtask.databinding.FragmentTablesScreenBinding
import com.quandoo.androidtask.presentation.viewmodel.RestaurantViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_tables_screen.*

@AndroidEntryPoint
class TablesScreen : Fragment(R.layout.fragment_tables_screen), Logger,
    TablesRvAdapter.OnTableItemClickListener {
    private var fragmentTablesBinding: FragmentTablesScreenBinding? = null
    private val viewModel by viewModels<RestaurantViewModel>()
    private lateinit var tablesAdapter: TablesRvAdapter
    private var dialog: AlertDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentTablesScreenBinding.bind(view)
        fragmentTablesBinding = binding

        initRecyclerView()
        viewModel.tables.observe(viewLifecycleOwner) { fetchedTables ->
            tablesAdapter.setTableData(fetchedTables)
            tablesAdapter.notifyDataSetChanged()
        }

        viewModel.customers.observe(viewLifecycleOwner) { fetchedCustomers ->
            tablesAdapter.setCustomerData(fetchedCustomers)
            tablesAdapter.notifyDataSetChanged()
        }
        viewModel.fetchTables()
        viewModel.fetchCustomers()
    }

    private fun initRecyclerView() {
        tablesAdapter = TablesRvAdapter(this)
        tables_recycler_view.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = tablesAdapter
        }
    }

    override fun onTableItemClick(clickedTable: Table) {
        if (clickedTable.reservedBy != null) {
            AlertDialog.Builder(requireActivity())
                .setMessage("Do you want to free the table?")
                .setPositiveButton("Yes") { _: DialogInterface?, _: Int ->
                    viewModel.cancelReservation(clickedTable)
                    tablesAdapter.notifyDataSetChanged()
                }.setNegativeButton("No", null)
                .show()

        } else {
            val action = TablesScreenDirections.actionTablesScreenToCustomersScreen(clickedTable)
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentTablesBinding = null
        dialog = null
    }

}