package com.quandoo.androidtask.presentation.views.customers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.quandoo.androidtask.R
import com.quandoo.androidtask.core.utils.Logger
import com.quandoo.androidtask.data.model.response.Customer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.customer_cell.view.*

internal class CustomersRvAdapter(
    private val customerClickListener: OnCustomerClickListener
) :
    RecyclerView.Adapter<CustomersRvAdapter.CustomerViewHolder>(),
    Logger {

    private var customers: List<Customer> = listOf()

    fun setCustomerData(customerData: List<Customer>) {
        this.customers = customerData
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CustomerViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.customer_cell, viewGroup, false)
        return CustomerViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: CustomerViewHolder, i: Int) {
        val customer = this.customers[i]

        viewHolder.customerName.text = "${customer.firstName} ${customer.lastName}"
        Picasso.get().load(customer.imageUrl).into(viewHolder.customerAvatarImage)
    }

    override fun getItemCount(): Int = customers.size


    inner class CustomerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val customerName: TextView = itemView.customerNameTextView
        val customerAvatarImage: ImageView = itemView.customerAvatarImageView

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                customerClickListener.onCustomerClick(customers[position])
            }
        }
    }

    interface OnCustomerClickListener {
        fun onCustomerClick(customer: Customer)
    }
}