package com.quandoo.androidtask.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quandoo.androidtask.data.model.request.MakeReservation
import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.data.model.response.Table
import com.quandoo.androidtask.data.repository.RestaurantRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RestaurantViewModel @Inject constructor(private val restaurantRepo: RestaurantRepo) :
    ViewModel() {

    val tables = MutableLiveData<MutableList<Table>>()
    fun fetchTables() {
        tables.value = restaurantRepo.fetchTables()
    }

    fun reserveTable(makeReservation: MakeReservation) {
        restaurantRepo.makeReservation(makeReservation)
        fetchTables()
    }

    fun cancelReservation(reservedTable: Table) {
        restaurantRepo.cancelReservation(reservedTable)
        fetchTables()
    }

    val customers = MutableLiveData<MutableList<Customer>>()
    fun fetchCustomers() {
        customers.value = restaurantRepo.fetchCustomers()
    }
}