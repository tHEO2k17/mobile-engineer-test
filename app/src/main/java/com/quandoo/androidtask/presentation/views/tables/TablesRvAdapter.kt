package com.quandoo.androidtask.presentation.views.tables

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.quandoo.androidtask.R
import com.quandoo.androidtask.core.utils.Logger
import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.data.model.response.Table
import com.quandoo.androidtask.presentation.views.tables.TablesRvAdapter.TableViewHolder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.table_cell.view.*

class TablesRvAdapter internal constructor(
    private val itemClicked: OnTableItemClickListener
) : RecyclerView.Adapter<TableViewHolder>(), Logger {

    private var tables: List<Table> = listOf()
    private var customers: List<Customer> = listOf()

    fun setTableData(tableData: List<Table>) {
        this.tables = tableData
    }

    fun setCustomerData(customerData: List<Customer>) {
        this.customers = customerData
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): TableViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.table_cell, viewGroup, false)

        return TableViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: TableViewHolder, position: Int) {

        val table = tables[position]

        viewHolder.apply {
            tableId.text = "${table.id}"
            //TODO : Set name and color depending on reservation status
            if (table.reservedBy != null) {
                reservingCustomerName.text = table.reservedBy
                reservingCustomerName.setTextColor(Color.RED)

                //load reserving user image
                Picasso.get().load(findUserImage(table.reservedBy)).into(avatarImage)
                avatarImage.visibility = View.VISIBLE
            } else {
                reservingCustomerName.text = "Free"
                reservingCustomerName.setTextColor(Color.GREEN)
                avatarImage.visibility = View.INVISIBLE
            }
            tableImage.setImageResource(getTableShapeImageResourceId(table.shape))
            itemView.setOnClickListener { itemClicked.onTableItemClick(table) }
        }

    }

    override fun getItemCount(): Int = tables.size

    inner class TableViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val tableId: TextView = itemView.tableId
        val reservingCustomerName: TextView = itemView.reservingCustomerName
        val tableImage: ImageView = itemView.tableImageView
        val avatarImage: ImageView = itemView.avatarImageView

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                itemClicked.onTableItemClick(tables[position])
            }
        }
    }

    private fun findUserImage(userFirstNameLastName: String?): String? {
        for (customer in customers) {
            val fullName = "${customer.firstName} ${customer.lastName}"
            if (fullName == userFirstNameLastName) return customer.imageUrl
        }

        return null
    }

    private fun getTableShapeImageResourceId(tableShape: String): Int {
        return when (tableShape) {
            "circle" -> R.drawable.ic_circle
            "square" -> R.drawable.ic_square
            else -> R.drawable.ic_rectangle
        }
    }

    interface OnTableItemClickListener {
        fun onTableItemClick(clickedTable: Table)
    }
}