package com.quandoo.androidtask.presentation.views.customers

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.quandoo.androidtask.R
import com.quandoo.androidtask.core.utils.Logger
import com.quandoo.androidtask.data.model.request.MakeReservation
import com.quandoo.androidtask.data.model.response.Customer
import com.quandoo.androidtask.databinding.FragmentCustomersScreenBinding
import com.quandoo.androidtask.presentation.viewmodel.RestaurantViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_customers_screen.*

@AndroidEntryPoint
class CustomersScreen : Fragment(R.layout.fragment_customers_screen),
    CustomersRvAdapter.OnCustomerClickListener, Logger {
    private var fragmentCustomersBinding: FragmentCustomersScreenBinding? = null
    private val viewModel by viewModels<RestaurantViewModel>()
    private lateinit var customersAdapter: CustomersRvAdapter
    private val args: CustomersScreenArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentCustomersScreenBinding.bind(view)
        fragmentCustomersBinding = binding

        initRecyclerView()
        viewModel.customers.observe(viewLifecycleOwner) { customers ->
            customersAdapter.setCustomerData(customers)
            customersAdapter.notifyDataSetChanged()
        }
        viewModel.fetchCustomers()
    }

    private fun initRecyclerView() {
        customersAdapter = CustomersRvAdapter(this)
        customers_recycler_view.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = customersAdapter
        }
    }

    override fun onCustomerClick(customer: Customer) {
        viewModel.reserveTable(MakeReservation(args.selectedTable, customer))
        findNavController().navigateUp()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentCustomersBinding = null
    }

}